<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <link rel="stylesheet" href="../../../public/css/main.css"> -->
  <title>newGame</title>
  <style>
    html, body 
    {
      box-sizing: border-box;
      background-color: #fff;
      color: #636b6f;
      font-family: 'Nunito', sans-serif;
      font-weight: 200;
      height: 100vh;
      margin: 0;
    }
    .title 
    {
      font-size: 25px;
      text-align: center;
    }
  </style>
</head>
<body>
  <h1 class='title'> @yield('alert_title') No se encontro registro.</h1>
</body>
</html>