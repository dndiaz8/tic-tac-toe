<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class partidas extends Model
{
    // use Uuid;
    protected $fillable = 
    [
        'uuid', 'name', 'active'
    ];

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

}
