<?php

namespace App\Http\Controllers;

use App\partidas;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PartidasController extends Controller
{
    public function newGame()
    {
        $uuid = ['uuid'=>Str::uuid()];
        return view('partida',$uuid);
    }

    public function joinGame()
    {
        return view('unirse');
    }

    
}
